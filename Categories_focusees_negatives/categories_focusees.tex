
\include{preambule}
\usepackage{mathpartir}
\renewcommand{\pd}{\backslash}

\newcommand{\jute}[3]{#1\vdash #2\colon #3}
\newcommand{\june}[5]{#1\mid #2\colon #3\vdash #4\Downarrow #5}
\newcommand{\juvalsub}[7]{#1\vdash #2[#3\pd #4\colon #5]=#6\colon #7}
\newcommand{\junesubx}[7]{#1\mid #3\colon #5\vdash #2[#3\pd #4\colon #5]=#6\colon #7}
\newcommand{\junesuby}[9]{#1\mid #2\colon #3\vdash #4[#5\pd #6\colon #7]=#8\Downarrow #9}


\begin{document}
\title{Modèle catégorique du \( \lambda-\)calcul négatif focusé}
\maketitle

\section{Syntaxe}

\subsection{Le \( \lambda-\)calcul négatif focusé}
On se place dans un \( \lambda-\)calcul négatif simplement typé. Pour les types, on a la grammaire \[ A::=\alpha,\beta,\gamma\ldots\quad|\quad A\to A\quad|\quad A\times A \] et pour les termes la grammaire \[ t::=x,y,z\ldots\quad|\quad\lambda x.t\quad|\quad t\ t\quad|\quad (t,t)\quad|\quad\pi_i(t) \] avec les règles habituelles \[ (\lambda x.t)u=_\beta t\{x\pd u\}\qquad \pi_i(t_1,t_2)=_\beta t_i \] \[ t\colon A\to B=_\eta \lambda x.(tx)\qquad t\colon A\times B=_\eta(\pi_1(t),\pi_2(t)) \] ainsi que les règles de typage habituelles \begin{mathpar}
\inferrule[Var0]{\ }{\jute{\Gamma,x\colon A}xA}
\\
\inferrule[Abs0]{\jute{\Gamma,x\colon A}tB}{\jute\Gamma{\lambda x.t}{A\to B}}

\inferrule[App0]{\jute\Gamma t{A\to B}\and\jute\Gamma uA}{\jute\Gamma{t\ u}B}
\\
\inferrule[Pair0]{\jute\Gamma tA\and\jute\Gamma uB}{\jute\Gamma{(t,u)}{A\times B}}

\inferrule[Proj0]{\jute\Gamma t{A_1\times A_2}}{\jute\Gamma{\pi_i(t)}{A_i}}
\end{mathpar}
Où les contextes \( \Gamma \) sont définis par la grammaire \[ \Gamma::=\cdot\quad|\quad \Gamma,x\colon A \]
On s'intéresse aux formes focusées, c'est-à-dire aux valeurs données par la grammaire suivante, qui définit mutuellement récursivement les valeurs et les neutres : \[ v::=\lambda x.v\quad|\quad (v,v)\quad|\quad(n\colon\alpha) \] \[ n::=x\quad|\quad n\ v\quad|\quad \pi_i(n) \]
Il peut être montré (terminaison du \( \lambda-\)calcul typé) que chaque terme correctement typé est \( \beta\eta-\)équivalent à une unique telle valeur. On définit également un jugement de typage sur les valeurs et les neutres : \begin{mathpar}
\inferrule[Abs]{\jute{\Gamma,x\colon A}vB}{\jute\Gamma{\lambda x.v}{A\to B}}

\inferrule[Pair]{\jute\Gamma{v_1}A\and\jute\Gamma{v_2}B}{\jute\Gamma{(v_1,v_2)}{A\times B}}

\inferrule[Neutral]{x\colon N\in\Gamma\and \june\Gamma xNn\alpha}{\jute\Gamma n\alpha}
\\
\inferrule[Var]{\ }{\june\Gamma xNxN}

\inferrule[App]{\june\Gamma xNn{A\to B}\and\jute\Gamma vA}{\june\Gamma xN{n\ v}B}

\inferrule[Proj]{\june\Gamma xNn{A_1\times A_2}}{\june\Gamma xN{\pi_i(n)}{A_i}}
\end{mathpar}
Notons tout de suite qu'on a le lemme suivant, dont la démonstration est immédiate par induction sur le jugement de typage :\\
\lem{Pour tout contexte \( \Gamma \), toute variable \( x \), tout neutre \( n \) et tous types \( N \) et \( A \), si \[ \june\Gamma xNnA \] alors \( A \) est un sous-type de \( N \)}


\subsection{Fonctions auxiliaires}
On définit pour nous assister deux fonctions auxiliaires : l'\( \eta-\)expansion maximale, et la substitution héréditaire. La première prend en argument un neutre \( n \) et son type \( A \), et renvoie une valeur \( \eta_A(n) \) qui est \( \eta-\)équivalente à \( n \) (preuve immédiate par induction). Elle est définie par induction sur \( A \) :
\[ \eta_\alpha(n):=n \]\[ \eta_{A\to B}(n):=\lambda x.\eta_B(n\ \eta_A(x)) \]\[ \eta_{A\times B}(n):=(\eta_A(\pi_1(n)),\eta_B(\pi_2(n))) \] On s'autorisera également à écrire \( \eta_A(v) \) lorsque \( v \) est une valeur et \( A \) son type, défini comme \( \eta_A(v):=v \).

% Remarque [Gabriel]: $\eta_A(n)$ est l'\eta-expansion "maximale" de $n$.
% ON peut donner des noms aux deux opérations (si on veut): la première est une eta-expansion maximale,
% et la deuxieme une substitution héréditaire (c'est le nom standard dans la litérature, je crois).

La deuxième prend en argument une valeur \( v \), une variable \( x \), une valeur \( w \) et un contexte \( \Gamma \) tels que \begin{rom} 
%\item \( x\colon A\in\Gamma \) pour un certain type \( A \), 
\item \( \Gamma\vdash w\colon A \) pour un certain type \( A \), et 
\item \( \Gamma\vdash v\{x\pd w\}\colon B \) pour un certain type \( B \), 
\end{rom} et renvoie une valeur \( v[x\pd w]_\Gamma \), définie comme l'unique valeur \( v' \) telle que \( \juvalsub\Gamma vxwA{v'}B \) est dérivable dans le système qui suit, où on définit également mutuellement récursivement les jugements \( \junesubx\Gamma nxwAvB \) et \( \junesuby\Gamma yNnxwA{n'}B \) lorsque \( y\neq x \) : \begin{mathpar}
\inferrule[Abs]{\juvalsub{\Gamma, y\colon B}vxwA{v'}C}{\juvalsub\Gamma{(\lambda y.v)}xwA{\lambda y.v'}{B\to C}}\and
\inferrule[Pair]{\juvalsub\Gamma{v_1}xwA{v'_1}B\and\juvalsub\Gamma{v_2}xwA{v'_2}C}{\juvalsub\Gamma{(v_1,v_2)}xwA{(v'_1,v'_2)}{B\times C}}\and
\inferrule[NeutralX]{x\colon A\in\Gamma\and\junesubx\Gamma nxwAv\alpha}{\juvalsub\Gamma nxwAv\alpha}\and
\inferrule[NeutralY]{y\neq x\and y\colon N\in\Gamma\and\junesuby\Gamma yNnxwA{n'}\alpha}{\juvalsub\Gamma nxwA{n'}\alpha}\and
\inferrule[VarX]{\ }{\junesubx\Gamma xxwAwA}\and
\inferrule[VarY]{\ }{\junesuby\Gamma yByxwAyB}\and
\inferrule[AppX]{\junesubx\Gamma nxwA{\lambda y.u}{B\to C}\and \juvalsub\Gamma vxwA{v'}B\and \juvalsub\Gamma uy{v'}B{u'}C}{\junesubx\Gamma{(n\ v)}xwA{u'}C}\and
\inferrule[AppY]{\junesuby\Gamma yNnxwA{n'}{B\to C}\and \juvalsub\Gamma vxwA{v'}B}{\junesuby\Gamma yN{(n\ v)}xwA{n'\ v'}C}\and
\inferrule[ProjX]{\junesubx\Gamma nxwA{(v_1,v_2)}{B_1\times B_2}}{\junesubx\Gamma{\pi_i(n)}xwA{v_i}{B_i}}\and
\inferrule[ProjY]{\junesuby\Gamma yNnxwA{n'}{B_1\times B_2}}{\junesuby\Gamma yN{\pi_i(n)}xwA{\pi_i(n')}{B_i}}
\end{mathpar}


Pour montrer que \( v[x\pd w]_\Gamma \) existe, il suffit de constater que pour prouver \( \juvalsub\Gamma vxwA{v'}B \), \( \junesubx\Gamma nxwAvB \) ou \( \junesuby\Gamma yNnxwA{n'}B \), une unique règle peut s'appliquer (selon la structure du terme à gauche du crochet) et cette règle détermine de manière unique le membre de droite de l'égalité. La terminaison de la procédure se prouve par décroissance de la structure de \( A,t_0 \) (dans l'ordre lexicographique) où \( t_0 \) est le terme à gauche du crochet (donc, la valeur \( v \) ou le neutre \( n \)). En effet, toutes les prémisses voient \( t_0 \) diminuer et \( A \) demeurer inchangé, à l'exception de la troisième prémisse de {\tt AppX} où le type est \( B \) qui est sous-type strict de \( A \), puisque par la première prémisse de {\tt AppX}, \( \Gamma \mid x\colon A\vdash \_[\_]=\_ \Downarrow B\to C \), et le lemme 1.1.1 permet indique alors que \( B\to C \) est un sous-type de \( A \). \\
On montre alors par récurrence immédiate que \[ v[x\pd w]_\Gamma=_{\beta\eta}v\{x\pd w\} \] où le membre de droite n'est pas toujours une valeur (le membre de gauche, si).\\ Lorsque \( x \) est une variable, \( w \) une valeur et \( n \) un neutre, on s'autorisera à écrire \( n[x\pd w]_{\Gamma} \) pour l'unique valeur \( v \) telle que \( \junesubx\Gamma nxwAvB \) ou l'unique neutre \( n' \) tel que \( \junesuby\Gamma yNnxwA{n'}B \). On montre alors de manière immédiate que \[ n[x\pd w]_{\Gamma}=_{\beta\eta}n\{x\pd w\} \]
\subsection{Lemmes structurels}
On aura besoin des lemmes suivants dans le modèle catégorique. Le premier est évident :
\lem{Pour tout contexte \( \Gamma \), toutes valeurs \( v \) et \( w \) et toute variable \( x \), si \( x \) n'est pas libre dans \( v \), alors \[ v[x\pd w]_\Gamma=v \]}
Le suivant contient des identités entre l'\(\eta-\)expansion et la substitution héréditaire, qui se prouvent mutuellement récursivement :
\lem{Pour tout contexte \( \Gamma \), pour tout neutre \( n \), toutes valeur \( v \) et \( w \) et toute variable \( x \), on a, si \( A \) et \( B \) sont les types respectifs de \( w \) et \( t\{x\pd w\} \) dans \( \Gamma \), \[ \eta_B(n[x\pd w]_{\Gamma})=\eta_B(n)[x\pd w]_\Gamma\qquad(a) \]\[ v[x\pd\eta_A(x)]_\Gamma=v\qquad(b) \]}
Pour démontrer ce lemme, on procède par induction sur \( A \). Soit donc un type \( A \) tel que les deux identités sont vérifiées pour tout sous-type strict de \( A \) (appelons HR0 cette hypothèse de récurrence).\\
Pour la première égalité, raisonnons par induction sur \( B \). L'égalité est triviale si \( B \) est un type de base. Si \( B \) est un type produit, alors \[ \eta_{B_1\times B_2}(n)[x\pd w]_\Gamma = (\eta_{B_1}(\pi_1(n)),\eta_{B_2}(\pi_2(n)))[x\pd w]_\Gamma=(\eta_{B_1}(\pi_1(n)[x\pd w]_\Gamma),\eta_{B_2}(\pi_2(n)[x\pd w]_\Gamma) \] en utilisant {\tt Pair} et l'hypothèse de récurrence sur \( B_1 \) et \( B_2 \), qui sont des sous-types stricts de \( B \). Si \( x \) n'est pas la variable de tête de \( n \), alors {\tt ProjY} permet de conclure. Sinon, \( n[x\pd w]_\Gamma \) est une valeur de type \( B_1\times B_2 \), d'où \[ \eta_{B_1\times B_2}(n[x\pd w]_\Gamma)=\eta_{B_1\times B_2}((v_1,v_2))=(v_1,v_2) \] et par {\tt ProjX}, \[ \eta_{B_i}(\pi_i(n)[x\pd w])=\eta_{B_i}(v_i)=v_i \] ce qui permet de conclure. 
Si \( B \) est un type flèche, alors \[ \eta_{B_1\to B_2}(n)[x\pd w]_\Gamma=(\lambda y.\eta_{B_2}(n\ \eta_{B_1}(y)))[x\pd w]=\lambda y.\eta_{B_2}((n\ \eta_{B_1}(y))[x\pd w]) \] en utilisant {\tt Abs} et l'hypothèse de récurrence sur \( B_2 \), qui est un sous-type strict de \( B \). Si \( x \) n'est pas la variable de tête de \( n \), alors {\tt AppY} permet de conclure. Sinon, \( n[x\pd w]_\Gamma \) est une valeur de type \( B_1\to B_2 \), d'où \[ \eta_{B_1\to B_2}(n[x\pd w]_\Gamma)=\eta_{B_1\to B_2}(\lambda y.u)=\lambda y.u \] Puis par le lemme 1.3.1, \( \eta_{B_1}(y)[x\pd w]_\Gamma=\eta_{B_1}(y) \), donc par {\tt AppX} \[ \eta_{B_2}((n\ \eta_{B_1}(y)) = \eta_{B_2}(u[y\pd\eta_{B_1}(y)]) = u[y\pd\eta_{B_1}(y)] \] ce qui permet de conclure par le (b) de l'hypothèse de récurrence HR0, puisque \( B_1 \) est un sous-type strict de \( A \) par le lemme 1.1.1. On a donc démontré la première égalité par récurrence sur \( B \).\\
Pour la deuxième égalité, montrons par récurrence sur le terme \( t \) à gauche du crochet qu'on a, pour toute variable \( x \), toute valeur \( v \) et tout neutre \( n \), \[ \sys{v[x\pd\eta_A(x)]_\Gamma&=v \\ n[x\pd\eta_A(x)]_\Gamma&=\sys{\eta_B(n)\quad&\text{si }x\text{ est la variable de tête de }n\\n\quad&\text{sinon}}} \] On fait une disjonction de cas sur la forme de \( t \), donc sur la dernière règle à appliquer pour prouver le jugement de substitution pertinent. Tous les cas se traitent facilement par hypothèse de récurrence (les prémisses des règles se font sur des sous-termes stricts de \( t \)), sauf le cas de la règle {\tt AppX}, où on effectue le raisonnement suivant : on cherche à montrer que \[ (n\ v)[x\pd\eta_A(x)]_\Gamma=\eta_C(n\ v) \] où \( n \) a le type \( B\to C \) et \( v \) a le type \( C \), et où \( x \) est la variable de tête de \( n \). On a par hypothèse de récurrence sur \( n \), \[ n[x\pd\eta_A(x)]_\Gamma=\eta_{B\to C}(n)=\lambda y.\eta_C(n\ \eta_B(y)) \] Puis l'hypothèse de récurrence sur \( v \) donne \[ v[x\pd\eta_A(x)]_\Gamma=v \] Donc par la règle {\tt AppX}, \[ (n\ v)[x\pd\eta_A(x)]_\Gamma=\eta_C(n\ \eta_B(y))[y\pd v]_\Gamma \] Or, \( B \) et \( C \) sont des sous-types stricts de \( A \) par le lemme 1.1.1, donc par le (a) de HR0 : \[ (n\ v)[x\pd\eta_A(x)]_\Gamma=\eta_C((n\ \eta_B(y)[y\pd v]_\Gamma) \] \( y \) étant créée fraîche dans la définition de \( \eta_{B\to C} \), elle n'est pas libre dans \( n \) donc la règle {\tt AppY} s'applique, et grâce au lemme 1.3.1, on a \( n[y\pd v]_\Gamma=n \), donc \[ (n\ v)[x\pd\eta_A(x)]_\Gamma=\eta_C(n\ (\eta_B(y)[y\pd v]_\Gamma)) \] Puis à nouveau par le (a) de HR0, \( \eta_B(y)[y\pd v]_\Gamma=\eta_B(y[y\pd v]_\Gamma)=\eta_B(v)=v \), d'où finalement \[ (n\ v)[x\pd\eta_A(x)]_\Gamma=\eta_C(n\ v) \]

Enfin, on aura besoin du lemme suivant :\\
\lem{Pour tout contexte \( \Gamma \), toutes variables \( x,x_1,\ldots,x_n \) et pour toutes valeurs \( v_1,\ldots,v_n,u \) telles que \( x \) n'est libre dans aucune des \( v_i \), on a \[ (x\ v_1\ \ldots\ v_n)[x\pd\lambda x_1.\ldots\lambda x_n.u]_\Gamma=u[x_1\pd v_1]_\Gamma\ldots[x_n\pd v_n]_\Gamma \]}
La démonstration se fait par récurrence sur \( n \). Le cas \( n=0 \) est trivial. Soit \( n \) un entier tel que le lemme soit vérifié pour \( n \) valeurs, puis soit \( \Gamma \) un contexte, \( x,x_1,\ldots,x_{n+1} \) des variables et \( v_1,\ldots,v_{n+1},u \) des valeurs telles que \( x \) n'est libre dans aucune des \( v_i \). En utilisant l'hypothèse de récurrence puis la règle {\tt Abs}, on a \[
(x\ v_1\ \ldots\ v_{n})[x\pd\lambda x_1.\ldots\lambda x_{n+1}.u]_\Gamma
= (\lambda x_{n+1}.u)[x_1\pd v_1]_\Gamma\ldots[x_n\pd v_n]_\Gamma = \lambda x_{n+1}.(u[x_1\pd v_1]_\Gamma\ldots [x_n\pd v_n]_\Gamma) \] 
Puis par le lemme 1.3.1, \( v_{n+1}[x\pd\lambda x_1.\ldots\lambda x_{n+1}.u]_\Gamma=v_{n+1} \), donc par la règle {\tt AppX}, on a \[ (x\ v_1\ \ldots\ v_{n+1})[x\pd\lambda x_1.\ldots\lambda x_{n+1}.u]_\Gamma = u[x_1\pd v_1]_\Gamma\ldots[x_n\pd v_n]_\Gamma[x_{n+1}\pd v_{n+1}]_\Gamma \] ce qu'il fallait démontrer.
\section{Modèle catégorique}

On définit la catégorie \EC de la manière suivante :\\
Ses objets sont les contextes \( \Gamma \).\\
Un morphisme de \( \Delta \) à \( \Gamma = (x_i\colon A_i)_{i\in I} \) est un tuple \( (v_i)_{i\in I} \) de valeurs tel que \[ \forall i\in I,\Delta\vdash v_i\colon A_i \] La composition de \( \sigma\colon\Delta\to\Gamma \) et de \( \tau\colon\Sigma\to\Delta \), où \( \Gamma=(x_i\colon A_i)_{i\in I} \) et \( \Delta=y_1\colon B_1,\ldots,y_n\colon B_n \) est définie par \[ \forall i\in I,\;(\sigma\circ\tau)_i=\sigma_i[y_1\pd\tau_1]_\Sigma\ldots[y_n\pd\tau_n]_\Sigma \]
L'identité de \( \Gamma=(x_i\colon A_i)_{i\in I} \) est alors définie par \[ \forall i\in I,(\ID_\Gamma)_i=\eta_{A_i}(x_i) \] C'est bien un neutre pour la composition en vertu du lemme 1.3.2.(b)\\
On montre facilement que \EC est munie de produits, puisque le contexte \( \Gamma,\Delta \) (concaténation des deux contextes \( \Gamma \) et \( \Delta \)) vérifie les propriétés caractéristiques qui définissent le produit de \( \Gamma \) et \( \Delta \). Comme il est courant en théorie des catégories, lorsque \( \sigma\colon\Sigma\to\Gamma \) et \( \tau\colon\Theta\to\Delta \), où \( \Gamma=(x_i\colon A_i)_{i\in I} \) et \( \Delta=(y_j\colon B_j)_{j\in J} \), on notera \( \sigma\times\tau \) le morphisme de \( \Sigma\times\Theta \) vers \( \Gamma\times\Delta \) défini par \[ \sys{\forall i\in I,(\sigma\times\tau)_i&=\sigma_i\\\forall j\in J,(\sigma\times\tau)_j&=\tau_j} \] Lorsque \( \Theta=\Delta \) et que \( \tau=\ID_\Delta \), on s'autorisera à écrire \( \sigma\times\Delta \) pour \( \sigma\times\ID_\Delta \). \\
Montrons à présent que \EC est dotée d'exponentielles. Définissons \( \Delta^\Gamma \), où \( \Gamma=x_1\colon A_1,\ldots,x_n\colon A_m \) et \( \Delta=(y_j\colon B_j)_{j\in J} \), par \[ \Delta^\Gamma:=(f_j\colon A_1\to\ldots\to A_m\to B_j)_{j\in J} \] et montrons que cette définition fournit bien une exponentielle pour la fonction d'évaluation \( \epsilon\colon \Delta^\Gamma\times\Gamma\to\Delta \) et la fonction d'abstraction \( \lambda \) qui à \( \sigma\colon \Sigma\times\Gamma\to\Delta \) associe \( \lambda\sigma\colon\Sigma\to\Delta^\Gamma \) définies comme suit, lorsque \( \Gamma=x_1\colon A_1,\ldots,x_m\colon A_m \) et \( \Delta=(y_j\colon B_j)_{j\in J} \) :
 \[ \forall j\in J,\epsilon_j:=\eta_{B_j}(f_j\ \eta_{A_1}(x_1)\ \ldots\ \eta_{A_m}(x_m)) \]
 \[ \forall \sigma\colon \Sigma\times\Gamma\to\Delta,\forall j\in J,\;(\lambda\sigma)_j:=\lambda x_1.\ldots\lambda x_m.\sigma_j \]
Pour ceci, montrons que pour tout \( \sigma\colon \Sigma\times \Gamma\to\Delta \), \[ \sigma=\epsilon\circ(\lambda\sigma\times \Gamma)\qquad(1) \] puis que pour tout \( \rho\colon \Sigma\to\Delta^\Gamma \), \[ \lambda(\epsilon\circ(\rho\times \Gamma))=\rho\qquad(2) \] ce qui prouvera l'unicité de \( \lambda\sigma \) comme morphisme qui vérifie (1), puisque si \( \rho \) est un morphisme qui vérifie \( \sigma=\epsilon\circ(\rho\times\Gamma) \), alors en appliquant \( \lambda \) et (2), on retrouve \( \lambda\sigma=\rho \).\\
Pour montrer (1), soit \( \sigma\colon\Sigma\times\Gamma\to\Delta \), avec \( \Gamma=x_1\colon A_1,\ldots,x_m\colon A_m \) et \( \Delta=(y_j\colon B_j)_{j\in J}=y_1\colon B_1,\ldots,y_n\colon B_n \). Pour tout \( j\in J \), on a :
\[ \begin{aligned}
(\epsilon\circ(\lambda\sigma\times\Gamma))_j
&= \epsilon_j[f_1\pd(\lambda\sigma)_1]_{\Sigma\times\Gamma}\ldots[f_n\pd(\lambda\sigma)_n]_{\Sigma\times\Gamma}[x_1\pd\eta_{A_1}(x_1)]_{\Sigma\times\Gamma}\ldots[x_m\pd\eta_{A_m}(x_m)]_{\Sigma\times\Gamma} \\
&= \epsilon_j[f_j\pd(\lambda\sigma)_j]_{\Sigma\times\Gamma}\qquad\text{(lemme 1.3.1 et 1.3.2(b))}\\
&= \eta_{B_j}(f_j\ \eta_{A_1}(x_1)\ \ldots\ \eta_{A_m}(x_m))[f_j\pd\lambda x_1.\ldots\lambda x_m.\sigma_j]_{\Sigma\times\Gamma}\\
&= \eta_{B_j}((f_j\ \eta_{A_1}(x_1)\ \ldots\ \eta_{A_m}(x_m))[f_j\pd\lambda x_1.\ldots\lambda x_m.\sigma_j]_{\Sigma\times\Gamma})\qquad\text{(lemme 1.3.2.(a))}\\
&= \eta_{B_j}(\sigma_j[x_B\pd\eta_{A_1}(x_1)]_{\Sigma\times\Gamma}\ldots[x_m\pd\eta_{A_m}(x_m)]_{\Sigma\times\Gamma})\qquad\text{(lemme 1.3.3)}\\
&= \eta_{B_j}(\sigma_j)\qquad\text{(lemme 1.3.2.(b)}\\
&= \sigma_j
\end{aligned} \]
Pour montrer (2), soit \( \rho\colon\Sigma\to\Delta^\Gamma \), avec \( \Gamma=x_1\colon A_1,\ldots, x_m\colon A_m\) et \( \Delta=(y_j\colon B_j)_{j\in J}=y_1\colon B_1,\ldots,y_n\colon B_n\). Pour tout \( j\in J \), on a
\[ \begin{aligned}
(\lambda(\epsilon\circ(\rho\times\Gamma)))_j
&=\lambda x_1.\ldots\lambda x_m.(\epsilon\circ(\rho\times\Gamma))_j \\
&=\lambda x_1.\ldots\lambda x_m.(\epsilon_j[f_1\pd\rho_1]_{\Sigma\times\Gamma}\ldots[f_n\pd\rho_n]_{\Sigma\times\Gamma}[x_1\pd\eta_{A_1}(x_1)]_{\Sigma\times\Gamma}\ldots[x_m\pd\eta_{A_m}(x_m)]_{\Sigma\times\Gamma}) \\
&= \lambda x_1.\ldots\lambda x_m.(\epsilon_j[f_j\pd\rho_j]_{\Sigma\times\Gamma})\qquad\text{(lemme 1.3.1 et 1.3.2.(b))} \\
&= \lambda x_1.\ldots.\lambda x_m.(\eta_{B_j}(f_j\ \eta_{A_1}(x_1)\ \ldots\ \eta_{A_m}(x_m))[f_j\pd\rho_j]_{\Sigma\times\Gamma}) \\
&= (\lambda x_1.\ldots.\lambda x_m.\eta_{B_j}(f_j\ \eta_{A_1}(x_1)\ \ldots\ \eta_{A_m}(x_m)))[f_j\pd\rho_j]_{\Sigma\times\Gamma}\qquad\text{(règle }\texttt{Abs}\text{)}\\
&= (\eta_{A_1\to\ldots\to A_m\to B_j}(f_j))[f_j\pd\rho_j]_{\Sigma\times\Gamma}\\
&= \eta_{A_1\to\ldots\to A_m\to B_j}(f_j[f_j\pd\rho_j]_{\Sigma\times\Gamma})\qquad\text{(lemme 1.3.2.(a))}\\
&= \eta_{A_1\to\ldots\to A_m\to B_j}(\rho_j) \\
&= \rho_j 
\end{aligned} \] ce qui prouve que \EC a des exponentielles.
\end{document}